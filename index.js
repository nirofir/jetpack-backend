const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const mongoose = require('mongoose');
const router = require('./src/router');

require('dotenv').config();

mongoose.connect(`mongodb://${process.env.MONGO_SERVER_PROD}:${process.env.MONGO_PORT_PROD}/${process.env.MONGO_DBNAME_PROD}`);

const allowedOrigins = [
  "https://jetpack-master.netlify.app",
  "http://localhost:3000",
  "http://localhost",
  "http://159.203.184.172",
  "http://localhost:8301",
  "https://jetpackapps.co",
  "https://www.jetpackapps.co",
  "https://app.jetpackapps.co",
  "https://www.app.jetpackapps.co",
  "https://d53645e82bcd.ngrok.io",
];

app.use(
  cors({
    origin: function (origin, callback) {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      // if (allowedOrigins.indexOf(origin) === -1) {
      //   console.log(origin)
      //   const msg =
      //     "The CORS policy for this site does not " +
      //     "allow access from the specified Origin.";
      //   return callback(new Error(msg), false);
      // }
      return callback(null, true);
    },
  })
);



// parse application/json
app.use(bodyParser.json({ limit: '50mb' }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// Routing
router(app);

app.listen(process.env.PORT || 4000, () => {
  console.log("Server is running on 4000 port");
});
