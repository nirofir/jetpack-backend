const Token = require("./../models/token");
const { getBoardDetails, getClientUserDetails } = require("./../api/monday");
var jwt = require('jsonwebtoken');

/**
 * saveToken
 *
 * @method POST
 *
 * @param { token: String } req
 * @param { status: Number, msg: String } res
 * @param { status: Number, data: Object } next
 */
exports.saveToken = async (req, res, next) => {
  const body = req.body;
  // request validation
  if (!body.token) {
    return res.send({
      status: 403,
      msg: "Missing Token",
    });
  }

  // const { userId, accountId, backToUrl } = jwt.verify(body.token, process.env.CLIENT_SECRET);
  // console.log(userId, accountId, backToUrl)

  const user = await getClientUserDetails(body.token);
  console.log(user);

  // Validation Success
  var token = new Token({
    userId: user.account_id,
    token: body.token,
  });

  token.save((err, resp) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Saving Token has got some problems",
      });
    }
    return res.send({
      status: 200,
      data: resp,
    });
  });
};

/**
 * getToken
 *
 * @method post
 *
 * @param {*} req
 * @param { status: Number, msg: String } res
 * @param { status: Number, data: Object } next
 */
exports.getToken = (req, res, next) => {
  
  if(!req.body.userId) {
    return res.send({
      status: 400,
      msg: "no user Id",
    });
  }

  Token.find({ userId: req.body.userId }).then((resp, err) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Getting Token has got some problems",
      });
    }
    if (resp.length > 0) {
      return res.send({
        status: 200,
        data: resp[0],
      });
    } else {
      return res.send({
        status: 400,
        msg: "Token table is empty",
      });
    }
  });
};
