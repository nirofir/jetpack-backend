const Token = require("./../models/token");

const axios = require("axios");
const { getBoardDetails, getClientUserDetails,updateField } = require("./../api/monday");
var jwt = require('jsonwebtoken');

/**
 * generateToken
 *
 * @method POST
 *
 * @param { code: String, redirect_uri: String } req
 * @param {*} res
 * @param {*} next
 */
exports.generateToken = async (req, res, next) => {
  console.log("Generating Token...");

  const code = req.query.code;
  const state = req.query.state;
  const redirect_uri = req.query.redirect_uri;

  console.log("Code:", code);
  console.log("Redirect URI: ", redirect_uri);

  try {
    const response = await axios.post("https://auth.monday.com/oauth2/token", {
      code: code,
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      redirect_uri: redirect_uri,
    });

    // const { userId, accountId, backToUrl } = jwt.verify(code, process.env.CLIENT_SECRET);
   var  decoded = jwt.decode(response.data.access_token, {complete: true});

   try{

    Token.findOneAndUpdate({ userId: decoded.payload.uid}, 
      { token: response.data.access_token , userId: decoded.payload.uid }, 
      {upsert: true}, (err,resp) => {
        if(err) throw err
        return res.send({
          status: 200,
          token: response.data.access_token,
        });
    })
    }catch{
        return res.send({
          status: 500,
          msg: "Saving Token has got some problems",
        }); 
    }

  } catch (err) {
    return res.send({
      status: 500,
      msg: err,
    });
  }
};

/**
 * getBoard
 *
 * @method POST
 *
 * @param { token: String } req
 * @param { items: Object, columns: Object } res
 * @param {*} next
 */
exports.getBoard = async (req, res, next) => {
  console.log("Getting Token from Board...");

  const token = req.body.token;
  console.log("fetch board details: ", req.body.boardId);
  console.log("Token: ", token);
  var  decoded = jwt.decode(token, {complete: true});
  console.log(decoded.payload.uid)
  try{
     const response = await getBoardDetails(token,req.body.boardId);
      if (response) {
        return res.send({
          status: 200,
          columns: response.columns,
          items: response.items,
          userId : decoded.payload.uid
        });
      }
  }
  catch(e){
    console.log(e)
    return res.send({
      status: 500,
      msg: "Not able to get data from Monday",
    });
  }
};

/**
 * fetchBoard
 *
 * @method GET
 *
 * @param { } req
 * @param {*} res
 * @param {*} next
 */
// exports.fetchBoard = async (req, res, next) => {
//   console.log("--------------------------------");
//   console.log("Fetching Monday Data from database");
//   // const monday = mondaySdk();

//   const token = req.body.token;
//   // monday.setToken(token);
//   console.log("Token fetched: ", token);
//   console.log("fetch board details: ", req.body.boardId);

//   try{
//     const boardResponse = await getBoardDetails(token);
//     if (boardResponse) {
//       return res.send({
//         status: 200,
//         data: boardResponse,
//       });
//     }
//   }catch(e){
//     console.log(e)
//     return res.send({
//       status: 500,
//       msg: "Not able to get data from Monday.com",
//     });
//   }
// };

/**
 * getPerson
 *
 * @method POST
 *
 * @param { ids: String } req
 * @param {*} res
 * @param {*} next
 */
exports.getPerson = async (req, res, next) => {
  console.log("Getting Person detail");

  const ids = req.body.ids;
  try {
    const response = await monday.api(getUserDetailQuery(ids));
    return res.send({
      status: 200,
      data: response.data.users,
    });
  } catch (err) {
    return res.send({
      status: 500,
      msg: err,
    });
  }
};
/**
 * getPerson
 *
 * @method POST
 *
 * @param { ids: String } req
 * @param {*} res
 * @param {*} next
 */

exports.updateField = async (req, res, next) => {
  console.log("updating field in monday");
  const token = req.body.token;

  // const id = req.body.id;
  try {
    const response = await updateField(token,req.body.updateBody);
    return res.send({
      status: 200,
      data: response.data,
    });
  } catch (err) {
    return res.send({
      status: 500,
      msg: err,
    });
  }
};
