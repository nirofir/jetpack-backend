const User = require("./../models/users");

exports.registerUser = (req, res, next) => {
  const body = req.body;
  // request validation
  if (!body.token) {
    return res.send({
      status: 403,
      msg: "Missing Token",
    });
  }

  // Validation Success
  var user = new User({
    userId: body.userId,
    token: body.token,
  });

  user.save((err, resp) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Saving Token has got some problems",
      });
    }
    return res.send({
      status: 200,
      data: resp,
    });
  });
};

exports.getUser = (req, res, next) => {
  User.find({ user: req.body.userId, token: req.body.token }).then((resp, err) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Getting Token has got some problems",
      });
    }
    if (resp.length > 0) {
      return res.send({
        status: 200,
        data: resp[0],
      });
    } else {
      return res.send({
        status: 400,
        msg: "Token table is empty",
      });
    }
  });
};
