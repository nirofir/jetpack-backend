const Style = require("./../models/style");
var jwt = require('jsonwebtoken');

exports.getStyle = async (req, res, next) => {
  const boardId = req.params.boardId
  const boardName = req.params.boardName || undefined;
  console.log("Getting styles from database",boardId,boardName);

  // if(!req.params.boardName || !req.params.boardId) {
  //   return res.send({
  //     status: 403,
  //     msg: "Missing fields",
  //   });
  // }

  Style.find({boardId : boardId , boardName : boardName}).then((resp, err) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Not able to get Styles",
      });
    }
    if (resp.length > 0) {
      return res.send({
        status: 200,
        style: resp[0].style,
      });
    }
    return res.send({
      status: 400,
      msg: "Style table is empty",
    });
  });
};

exports.updateStyle = (req, res, next) => {
  console.log("Storing styles into database");

  // console.log(req.body.token)
  // if(!req.body.boardName || !req.body.boardId) {
  //   return res.send({
  //     status: 403,
  //     msg: "Missing fields",
  //   });
  // }
  // var  decoded = jwt.decode(JSON.parse(req.body.token));
  // console.log(decoded)
  const styleOne = JSON.parse(req.body.style);

  if (!styleOne) {
    return res.send({
      status: 403,
      msg: "Missing fields",
    });
  }

  console.log(req.body.boardId)
  Style.find({ boardId: req.body.boardId ,boardName : req.body.boardName  }).then((style, err) => {
    if (err) {
      return res.send({
        status: 500,
        msg: "Not able to save styles",
      });
    }
    console.log(style)
    if (style.length > 0) {
      const appStyle = style[0];
      appStyle.style = styleOne
      // appStyle.style =  style[0;
      appStyle.save({});
    } else {
      const appStyle = new Style({
        boardId: req.body.boardId ,
        style : styleOne,
        boardName : req.body.boardName
      });
      appStyle.save({});
    }

    return res.send({
      status: 200,
      msg: "Successfully updated",
    });
  });
};
