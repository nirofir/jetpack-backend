function updateFieldQuery(boardId,columId,itemId,updateString) {
    return `mutation {
        change_column_value (
          board_id: ${boardId}, 
          column_id : "${columId}",
          item_id : ${itemId},
          value : ${updateString}
        ){
					name
        }
    }`
  }
 function rateQuery(boardId,columId,itemId,update){
    return `mutation {
        change_column_value (
          board_id: ${boardId}, 
          column_id : "${columId}",
          item_id : ${itemId},
          value : ${update}
        ){
					name
        }
    }`

 }
 function voteQuery(){

 }
 function colorQuery(){

 }

exports.textMutation = updateFieldQuery;
exports.rateQuery = rateQuery;
exports.voteQuery = voteQuery;
exports.colorQuery = colorQuery;
exports.mutateChangeColumnValue = updateFieldQuery;


