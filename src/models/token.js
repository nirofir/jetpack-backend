const mongoose = require("mongoose");

const tokenSchema = new mongoose.Schema({
  userId: {
    type: String,
    // unique: true,
    required: true,
  },
  token: {
    type: String,
    unique: true,
    required: true,
  },
});

const TokenModel = mongoose.model("token", tokenSchema);

module.exports = TokenModel;
