const mongoose = require("mongoose");

const styleSchema = new mongoose.Schema({
  boardId: { type: String, required: true },
  boardName: {type: String},
  style: {
    type : Object
  },
});

const StyleModel = mongoose.model("style", styleSchema);

module.exports = StyleModel;
