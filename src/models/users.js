const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  userId: {
    type: String,
    unique: true,
    required: true,
  },
  teamId: {
    type: String,
    required: true,
  },
});

const UsersModel = mongoose.model("user", userSchema);

module.exports = UsersModel;
