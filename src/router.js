const TokenController = require("./controllers/token");
const MondayController = require("./controllers/monday");
const StyleController = require("./controllers/style");
const UserController = require("./controllers/user");

module.exports = (app) => {
  // Monday Token Controller
  app.post("/monday/saveToken", TokenController.saveToken);
  app.post("/monday/token", TokenController.getToken);

  // Monday Controller
  app.post("/", MondayController.generateToken);
  app.post("/board", MondayController.getBoard);
  app.post("/person", MondayController.getPerson);
  // app.post("/monday/board", MondayController.fetchBoard);
  app.post("/monday/update", MondayController.updateField);


  // Style Controller
  app.get("/style/:boardId/:boardName", StyleController.getStyle);
  app.get("/style/:boardId", StyleController.getStyle);
  app.post("/style", StyleController.updateStyle);

  //users
  app.post;
};
