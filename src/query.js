function boardQuery(boardId) {
  return `query {
      boards(ids: [${parseInt(boardId)}]) {
        columns {
            title
            id
            type
            settings_str 
        }
            
        items {
            id
        }
      }
    }`;
}

function getItemDetailQuery(ids) {
  return `query {
        items(ids: [${ids}]) {
            name
            id
            assets {
                id
                public_url
            }
            column_values {
                id
                title
                text
                value
            }
        }
    }`;
}

function getUsersQuery() {
  return `query {
        users {
            id
            name
            photo_thumb
        }
    }`;
}

function getUserDetailQuery(ids) {
  return `query {
        users(ids: [${ids}]) {
            name
            photo_thumb
        }
    }`;
}

function meQuery() {
  return `query (){
        me {
        is_guest
        join_date
        }
        }`;
}
exports.meQuery = meQuery;
exports.boardQuery = boardQuery;
exports.getUsersQuery = getUsersQuery;
exports.getItemDetailQuery = getItemDetailQuery;
exports.getUserDetailQuery = getUserDetailQuery;
