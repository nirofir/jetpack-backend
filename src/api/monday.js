const mondaySdk = require("monday-sdk-js");
const { boardQuery, getItemDetailQuery, getUsersQuery, meQuery } = require("./../query");
const {mutateChangeColumnValue} = require("./../mutations")

async function getItemDetails(ids, token) {
  const monday = mondaySdk();
  monday.setToken(token);

  try {
    const itemResponse = await monday.api(getItemDetailQuery(ids));
    const usersResponse = await monday.api(getUsersQuery());
    let items = [];
    const users = usersResponse.data.users;
    for (item of itemResponse.data.items) {
      const assets = item.assets;
      let newItem = {
        name: {
          text: item.name,
          value: item.name,
        },
        "item_id": {
          text : item.id,
          value : item.id
        }
      };

      for (column of item.column_values) {
        const value = JSON.parse(column.value);
        if (value && value.files && value.files[0].fileType === "ASSET") {
          for (asset of assets) {
            if (parseInt(asset.id, 10) === value.files[0].assetId) {
              newItem = {
                ...newItem,
                [column.id]: {
                  text: asset.public_url,
                  value,
                },
              };
            }
          }
        } else if (value && value.personsAndTeams) {
          let teams = [];
          for (person of value.personsAndTeams) {
            for (user of users) {
              if (person.id == user.id) teams.push(user);
            }
          }
          newItem = {
            ...newItem,
            [column.id]: {
              text: teams,
              value,
            },
          };
        } else {
          newItem = {
            ...newItem,
            [column.id]: {
              text: column.text,
              value,
            },
          };
        }
      }

      items.push(newItem);
    }

    return items;
  } catch (err) {
    return null;
  }
}

async function getBoardDetails(token,boardId) {
  const monday = mondaySdk();
  monday.setToken(token);
  // console.log(token, "wow")
  try {
    const boardResponse = await monday.api(boardQuery(boardId));
    // console.log(boardResponse.data.boards[0].columns)
    const columns = boardResponse.data.boards[0].columns;
    const columns2 = boardResponse.data.boards[0].items
    // console.log("ido")
    let itemIds = "";
    for (item of boardResponse.data.boards[0].items) {
      itemIds += `${item.id}, `;
    }
    console.log(boardResponse.data.boards[0].items)


    const items = await getItemDetails(itemIds, token);
    cc = columns.map(c => parseInt(c.id))

    if (items)
      return {
        columns,
        columns2,
        items,
      };
    else return null;
  } catch (err) {
    console.log(err)
    return null;
  }
}

async function getClientUserDetails(token) {
  const monday = mondaySdk();
  monday.setToken(token);
  return await monday.api(meQuery());
}

async function updateField(token,{boardId, columnId ,itemId  ,update}){
  const monday = mondaySdk();
  monday.setToken(token);
  let q = mutateChangeColumnValue(parseInt(boardId),columnId,itemId,JSON.stringify(JSON.stringify(update)))
  const updated =  await monday.api(q)
  console.log(updated)
  return 
}

exports.getClientUserDetails = getClientUserDetails;
exports.getItemDetails = getItemDetails;
exports.getBoardDetails = getBoardDetails;
exports.updateField = updateField;
